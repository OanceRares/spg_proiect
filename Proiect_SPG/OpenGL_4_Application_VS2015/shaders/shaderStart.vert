#version 410 core

layout(location=0) in vec3 vPosition;
layout(location=1) in vec3 vNormal;
layout(location=2) in vec2 vTexCoords;

out vec3 normal;
out vec4 fragPosEye;
out vec4 fragPosLightSpace;
out vec2 fragTexCoords;
out vec3 fragPos;
out vec3 cameraPos;

out vec3 reflectedVector;
out vec3 refractedVector;

flat out int isPoolModel;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightSpaceTrMatrix;
uniform vec3 cameraPosition;
uniform int isPool;

void main() 
{
	isPoolModel = isPool;
	//compute eye space coordinates
	fragPosEye = view * model * vec4(vPosition, 1.0f);
	normal = vNormal;
	fragTexCoords = vTexCoords;
	fragPosLightSpace = lightSpaceTrMatrix * model * vec4(vPosition, 1.0f);
	
	gl_Position = projection * view * model * vec4(vPosition, 1.0f);
	
	vec3 viewVector = normalize(vPosition - cameraPosition);
	
	
	cameraPos = cameraPosition;
	fragPos=vec3(model*vec4(vPosition,1.0f));
	
	reflectedVector = reflect(viewVector,vNormal);
	refractedVector = refract(viewVector,vNormal,1.0/1.33);
	
}