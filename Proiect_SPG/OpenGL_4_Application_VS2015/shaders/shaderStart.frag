#version 410 core

in vec3 normal;
in vec4 fragPosEye;
in vec4 fragPosLightSpace;
in vec2 fragTexCoords;
in vec3 fragPos;


in vec3 reflectedVector;
in vec3 refractedVector;
in vec3 cameraPos;

flat in int isPoolModel;

out vec4 fColor;

//lighting
uniform	mat3 normalMatrix;
uniform mat3 lightDirMatrix;
uniform	vec3 lightColor;
uniform	vec3 lightDir;
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform sampler2D shadowMap;
uniform samplerCube skybox;
uniform sampler2D normalMap;

vec3 norm;

vec3 ambient;
float ambientStrength = 0.2f;
vec3 diffuse;
vec3 specular;
float specularStrength = 0.5f;
float shininess = 64.0f;

struct PointLight {    
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;  

};
PointLight pointLight;

float computeFog()
{
	float fogDensity = 0.02f;
	float fragmentDistance = length(fragPosEye);
	float fogFactor = exp(-pow(fragmentDistance * fogDensity / 2, 2));
	
	return clamp(fogFactor, 0.0f, 1.0f);
}

void calcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // attenuation
    float distance    = length(light.position - fragPos);
    float att = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));   
	vec3 cameraPosEye = vec3(0.0f);
	//compute light direction
	vec3 lighrDir=normalize(light.position-fragPos);
	vec3 lightDirN = normalize(lightDirMatrix * lightDir);	
	//compute view direction 
	vec3 viewDirN = normalize(cameraPosEye - fragPosEye.xyz);
	//compute half vector
	vec3 halfVector = normalize(lightDirN + viewDirN);
	//transform normal
	vec3 normalEye = normalize(normalMatrix * normal);	
	//compute ambient light 
	ambient += att * ambientStrength * lightColor; 
	//compute diffuse light 
	diffuse += att * max(dot(normalEye, lightDirN), 0.0f) * lightColor;
	float specCoeff = pow(max(dot(halfVector, normalEye), 0.0f), shininess);
	specular += att * specularStrength * specCoeff * lightColor;
}

void computeLightComponents()
{		
	vec3 cameraPosEye = vec3(0.0f);//in eye coordinates, the viewer is situated at the origin
	
	//transform normal
	vec3 normalEye = normalize(normalMatrix * normal);	
	
	//compute light direction
	vec3 lightDirN = normalize(lightDirMatrix * lightDir);	

	//compute view direction 
	vec3 viewDirN = normalize(cameraPosEye - fragPosEye.xyz);
	
	//compute half vector
	vec3 halfVector = normalize(lightDirN + viewDirN);
		
	//compute ambient light
	ambient += ambientStrength * lightColor;
	
	//compute diffuse light
	diffuse += max(dot(normalEye, lightDirN), 0.0f) * lightColor;
	
	//compute specular light
	float specCoeff = pow(max(dot(halfVector, normalEye), 0.0f), shininess);
	specular += specularStrength * specCoeff * lightColor;
}

float computeShadow()
{	
	float shadow = 0.0f;
	// perform perspective divide
    vec3 normalizedCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    if(normalizedCoords.z > 1.0f)
        return 0.0f;
   
   // Transform to [0,1] range
    normalizedCoords = normalizedCoords * 0.5f + 0.5f;
   
   // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, normalizedCoords.xy).r;    
   
   // Get depth of current fragment from light's perspective
    float currentDepth = normalizedCoords.z;
   
   // Check whether current frag pos is in shadow
    float bias = max(0.01 * (1.0 - dot(normal,lightDir)),0.001);
	
	vec2 texelSize = 1.0 / textureSize(shadowMap,0);
	//PCF
	for(int x = -1; x<=1;++x){
		for(int y=-1;y<=1;++y){
			float pcfDepth = texture(shadowMap,normalizedCoords.xy + vec2(x,y) * texelSize).r;	
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
		}
	}

    return shadow/9.0;	
}

void main() 
{
	float shadow = 0.0001f;

		
	/*vec3 normalizedCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
	if(textureSize(normalMap,0).x > 0 ){
		norm = texture(normalMap, normalizedCoords.xy).rgb;
		norm = normalize(norm * 2.0 - 1.0);
	}*/
	
	computeLightComponents();
	
	pointLight.position=vec3(-4.51f,3.5f,0.07f);
	pointLight.constant=1.0f;
	pointLight.linear=0.0045f;
	pointLight.quadratic=0.0075f;
	
	vec3 viewD=vec3(0.0f)-fragPosEye.xyz;
		
	calcPointLight(pointLight,normal,fragPos,viewD);
	
	pointLight.position=cameraPos;
	pointLight.constant=1.0f;
	pointLight.linear=0.0045f;
	pointLight.quadratic=0.0075f;
	
	calcPointLight(pointLight,normal,fragPos,viewD);

	vec4 colorFromTexture = texture(diffuseTexture, fragTexCoords);
	if(colorFromTexture.a < 0.1)
		discard;
	
	ambient *= vec3(colorFromTexture); //modulate with diffuse map
	
	colorFromTexture = texture(diffuseTexture, fragTexCoords);
	if(colorFromTexture.a < 0.1)
		discard;
	diffuse *= vec3(colorFromTexture); //modulate with diffuse map
	
	colorFromTexture = texture(specularTexture, fragTexCoords);
	if(colorFromTexture.a < 0.1)
		discard;
	specular *= vec3(colorFromTexture); //modulate with specular map
	
	
	//modulate with shadow
	shadow = computeShadow();
	vec3 color = min((ambient + (1.0f - shadow)*diffuse) + (1.0f - shadow)*specular, 1.0f);
	float fogFactor = computeFog();
	vec3 fogColor = vec3(0.5f, 0.5f, 0.6f);
	vec3 result = fogColor * (1 - fogFactor) + color * fogFactor;

	fColor = vec4(result,1);
	

	
}
/*
	vec4 reflectedColor = texture(skybox,reflectedVector);
	fColor = mix(fColor,reflectedColor,0.5f);
/*
	vec4 refractedColor = texture(skybox,refractedVector);
	fColor = mix(fColor,refractedColor,1.0f);*/