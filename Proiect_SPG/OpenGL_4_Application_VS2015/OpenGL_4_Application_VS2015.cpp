#define GLEW_STATIC

#include <iostream>
#include "glm/glm.hpp"//core glm functionality
#include "glm/gtc/matrix_transform.hpp"//glm extension for generating common transformation matrices
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "GLEW/glew.h"
#include "GLFW/glfw3.h"
#include <string>
#include "Shader.hpp"
#include "Camera.hpp"
#define TINYOBJLOADER_IMPLEMENTATION
#include "Model3D.hpp"
#include "Mesh.hpp"
#include "Skybox.hpp"


extern "C"
{
	__declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;
}

int glWindowWidth = 1920;
int glWindowHeight = 1080;
int retina_width, retina_height;
int SENSITIVITY = 6;
int ok = 1;

bool hasJumped = false;
bool hasLanded = false;

GLFWwindow* glWindow = NULL;

const GLuint SHADOW_WIDTH = 4096, SHADOW_HEIGHT = 4096; 


glm::mat4 model;
glm::mat4 nanoSuitModel = glm::mat4(1.0f);
glm::mat4 view;
glm::mat4 projection;

glm::mat3 normalMatrix;
glm::mat3 lightDirMatrix;

GLuint normalMatrixLoc;
GLuint lightDirMatrixLoc;
GLuint cameraPosLoc;
GLuint projectionLoc;
GLuint viewLoc;
GLuint modelLoc;
GLuint isPoolLoc;

glm::vec3 lightDir;
glm::vec3 lightColor;
glm::vec3 modelPos;

GLuint lightDirLoc;
GLuint lightColorLoc;

glm::vec3 newCameraPos;
glm::vec3 oldCameraPos;
glm::vec3 grassPositions[1024];


gps::Camera myCamera(glm::vec3(-215.49f, 56.83f, 0.84f), glm::vec3(0.0, 0.0f, 0.0f));

GLfloat cameraSpeed = 0.1f;

bool pressedKeys[1024];

GLfloat angle;
GLfloat lightAngle;

gps::Model3D airplaneModel;
gps::Model3D ground;
gps::Model3D pool;
gps::Model3D cottage;
gps::Model3D lightCube;
gps::Model3D secondLightCube;
gps::Model3D tree[6];
gps::Model3D fenceModel;
gps::Model3D smallLampModel;
gps::Model3D grassModel[1024];
gps::Model3D tableModel;
gps::Model3D trashBinModel;

gps::Shader myCustomShader;
gps::Shader lightShader;
gps::Shader depthMapShader;

GLuint shadowMapFBO;
GLuint depthMapTexture;
GLuint textureID;

std::vector<const GLchar*> faces;

gps::SkyBox mySkyBox;
gps::Shader skyboxShader;

double pitch = 0.0f;
double yaw = -90.0f;

float planeSpeed = 0.0f;
float fallSpeed = 0.001f;
GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR)
	{
		std::string error;
		switch (errorCode)
		{
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	fprintf(stdout, "window resized to width: %d , and height: %d\n", width, height);
	//TODO
	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	myCustomShader.useShaderProgram();

	//set projection matrix
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	//send matrix data to shader
	GLint projLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

	lightShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	//set Viewport transform
	glViewport(0, 0, retina_width, retina_height);
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			pressedKeys[key] = true;
		else if (action == GLFW_RELEASE)
			pressedKeys[key] = false;
	}
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{

}

void processMovement()
{
	//rotate camera
	glfwGetCursorPos(glWindow, &yaw, &pitch);

	myCamera.rotate(-pitch / SENSITIVITY, yaw / SENSITIVITY);
	printf("\n pitch: %f \n yaw: %f \n XYZ: %f %f %f\n", pitch, yaw, myCamera.getCameraPosition().x, myCamera.getCameraPosition().y, myCamera.getCameraPosition().z);



	//rotate model
	if (pressedKeys[GLFW_KEY_Q]) {
		angle += 0.1f;
		if (angle > 360.0f)
			angle -= 360.0f;
	}

	if (pressedKeys[GLFW_KEY_E]) {
		angle -= 0.1f;
		if (angle < 0.0f)
			angle += 360.0f;
	}


	//move model
	if (pressedKeys[GLFW_KEY_UP]) {
		modelPos.z += 0.01f;
	}
	if (pressedKeys[GLFW_KEY_DOWN]) {
		modelPos.z -= 0.01f;
	}
	if (pressedKeys[GLFW_KEY_LEFT]) {
		modelPos.x += 0.01f;
	}
	if (pressedKeys[GLFW_KEY_RIGHT]) {
		modelPos.x -= 0.01f;
	}
	//move camera
	if (pressedKeys[GLFW_KEY_W]) {//daca noua pozitie a camerei, calculata cu formula din camera cpp(camera.move) ajunge sa treaca de niste coord x si z, atunci nu ma misc(sau pot chiar in camera.move sa verific)
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_R]) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	if (pressedKeys[GLFW_KEY_T]) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	if (pressedKeys[GLFW_KEY_Y]) {
		glPolygonMode(GL_FRONT_AND_BACK,GL_POINT);
	}
	if (pressedKeys[GLFW_KEY_F]) {
		hasJumped = true;
	}

	if (pressedKeys[GLFW_KEY_J]) {
		lightAngle += 0.3f;
		if (lightAngle > 360.0f)
			lightAngle -= 360.0f;
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		myCustomShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

	if (pressedKeys[GLFW_KEY_L]) {
		lightAngle -= 0.3f;
		if (lightAngle < 0.0f)
			lightAngle += 360.0f;
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		myCustomShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

	myCustomShader.useShaderProgram();
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	
}

bool initOpenGLWindow()
{
	
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}
	glfwWindowHint(GLFW_SAMPLES,8);
	glWindow = glfwCreateWindow(glWindowWidth, glWindowHeight, "OpenGL Shader Example", NULL, NULL);
	if (!glWindow) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	glfwSetWindowSizeCallback(glWindow, windowResizeCallback);
	glfwMakeContextCurrent(glWindow);

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	glfwSetKeyCallback(glWindow, keyboardCallback);
	glfwSetCursorPosCallback(glWindow, mouseCallback);
	glEnable(GL_MULTISAMPLE);
	//glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return true;
}

void initOpenGLState()
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	glViewport(0, 0, retina_width, retina_height);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	//glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise

}

void initFBOs()
{
	//generate FBO ID
	glGenFramebuffers(1, &shadowMapFBO);

	//create depth texture for FBO
	glGenTextures(1, &depthMapTexture);

	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//attach texture to FBO
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

glm::mat4 computeLightSpaceTrMatrix()
{
	const GLfloat near_plane = 1.0f, far_plane = 100.0f;
	glm::mat4 lightProjection = glm::ortho(-80.0f, 80.0f, -80.0f, 80.0f, near_plane, far_plane);

	glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
	glm::mat4 lightView = glm::lookAt(lightDirTr, myCamera.getCameraTarget(), glm::vec3(0.0f, 1.0f, 0.0f));

	return lightProjection * lightView;
}

void initModels()
{
	airplaneModel = gps::Model3D("objects/airplane/piper_pa18.obj", "objects/airplane/");
	fenceModel = gps::Model3D("objects/fence/fence.obj", "objects/fence/");
	cottage = gps::Model3D("objects/cottage/cottage.obj", "objects/cottage/");
	lightCube = gps::Model3D("objects/cube/cube.obj", "objects/cube/");
	secondLightCube = gps::Model3D("objects/cube/cube.obj", "objects/cube/");
	for (int i = 0; i < 6; i++) {
		tree[i] = gps::Model3D("objects/tree/Tree.obj", "objects/tree/");
	}


	for (int i = 0; i < 1024; i++) {
		grassModel[i] = gps::Model3D("objects/grass/grass.obj", "objects/grass/");
	}

	ground = gps::Model3D("objects/ground/ground.obj", "objects/ground/");
	pool = gps::Model3D("objects/pool/pool.obj", "objects/pool/");
	smallLampModel = gps::Model3D("objects/small lamp/lamp.obj", "objects/small lamp/");
	tableModel = gps::Model3D("objects/table/table.obj", "objects/table/");
	trashBinModel = gps::Model3D("objects/trash bin/trash_bin.obj", "objects/trash bin/");


	faces.push_back("textures/skybox/right.jpg");
	faces.push_back("textures/skybox/left.jpg");
	faces.push_back("textures/skybox/top.jpg");
	faces.push_back("textures/skybox/bottom.jpg");
	faces.push_back("textures/skybox/back.jpg");
	faces.push_back("textures/skybox/front.jpg");

}

void initShaders()
{
	myCustomShader.loadShader("shaders/shaderStart.vert", "shaders/shaderStart.frag");
	lightShader.loadShader("shaders/lightCube.vert", "shaders/lightCube.frag");
	depthMapShader.loadShader("shaders/simpleDepthMap.vert", "shaders/simpleDepthMap.frag");
}

void initUniforms()
{

	int zMax = 55;
	int zMin = -65;
	int xMin = -40;
	int xMax = 65;
	int xRand;
	int zRand;

	for (int i = 0; i < 1024; i++) {
		xRand = rand() % (xMax - xMin + 1) + xMin;
		zRand = rand() % (zMax - zMin + 1) + zMin;

		if ((zRand > -8 && zRand < -1) && (xRand > -58 && xRand < 14)) {
			zRand += 10;
			xRand = rand() % 50 + 14;
		}
		grassPositions[i] = glm::vec3(xRand,-0.1f,zRand);
	}

	myCustomShader.useShaderProgram();

	isPoolLoc = glGetUniformLocation(myCustomShader.shaderProgram, "isPool");
	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");

	viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");

	normalMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "normalMatrix");

	lightDirMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDirMatrix");

	cameraPosLoc = glGetUniformLocation(myCustomShader.shaderProgram,"cameraPosition");

	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	projectionLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(0.0f, 1.0f, 2.0f);
	lightDirLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));

	//set light color
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	lightShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	mySkyBox.Load(faces);
	skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
	skyboxShader.useShaderProgram();
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE,glm::value_ptr(projection));
	
	
}

void renderScene()
{
	fallSpeed += 0.002;
	planeSpeed += 0.2f;
	if (myCamera.getCameraPosition().y < 2.5f) {
		hasLanded = true;
		myCamera.lockYAxis();
	}
	if (!hasJumped) {
		myCamera.setCameraPosition(glm::vec3(myCamera.getCameraPosition().x + 0.2f, myCamera.getCameraPosition().y, myCamera.getCameraPosition().z));//cat timp nu am sarit, tot merg in fata
	}
	else if(!hasLanded) {
		myCamera.setCameraPosition(glm::vec3(myCamera.getCameraPosition().x + fallSpeed/2, myCamera.getCameraPosition().y - fallSpeed, myCamera.getCameraPosition().z));//daca am sarit, tot cad pana aterizez
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	processMovement();

	glCullFace(GL_FRONT);

	//render the scene to the depth buffer (first pass)xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	depthMapShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),1,GL_FALSE,glm::value_ptr(computeLightSpaceTrMatrix()));
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	//TREE
	for (int i = 0; i < 6; i++) {
		model = glm::translate(glm::mat4(1.0f), glm::vec3(-8.0f * (i+2), 0.0f, -2.0f));
		model = glm::rotate(model, glm::radians(angle), glm::vec3(0, 1, 0));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
		glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
		glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
		tree[i].Draw(depthMapShader);
	}
	//GROUND
	model = glm::translate(glm::mat4(1.0f), modelPos);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform1i(isPoolLoc, 0);
	ground.Draw(myCustomShader);

	//POOL
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1000.0f, -100.05f, 0.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	pool.Draw(depthMapShader);

	//COTTAGE
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -0.2f, -0.7f));
	model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),1,GL_FALSE,glm::value_ptr(model));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	cottage.Draw(depthMapShader);

	//AIRPLANE
	if (planeSpeed - 150 < 300) {

		model = glm::translate(glm::mat4(1.0f), glm::vec3(-200.0f + planeSpeed, 50.0f, 1.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
		model = glm::rotate(model, glm::radians(angle + 90), glm::vec3(0, 1, 0));
		glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
		glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
		airplaneModel.Draw(depthMapShader);
	}
	//FENCE
	model = glm::mat4(1.0f);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	fenceModel.Draw(depthMapShader);

	//SMALL LAMP
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.9f, -1.3f, -1.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	smallLampModel.Draw(depthMapShader);

	//TABLE
	model = glm::mat4(1.0f);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	tableModel.Draw(depthMapShader);

	//TRASH BIN
	model = glm::mat4(1.0f);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	trashBinModel.Draw(depthMapShader);

	glCullFace(GL_BACK);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//render the scene (second pass)xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	myCustomShader.useShaderProgram();

	//send lightSpace matrix to shader
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "lightSpaceTrMatrix"),1,GL_FALSE,glm::value_ptr(computeLightSpaceTrMatrix()));
	//send view matrix to shader
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "view"),1,GL_FALSE,glm::value_ptr(view));
	//compute light direction transformation matrix
	lightDirMatrix = glm::mat3(glm::inverseTranspose(view));
	//send lightDir matrix data to shader
	glUniformMatrix3fv(lightDirMatrixLoc, 1, GL_FALSE, glm::value_ptr(lightDirMatrix));
	glViewport(0, 0, retina_width, retina_height);
	myCustomShader.useShaderProgram();
	//bind the depth map
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "shadowMap"), 3);


	//AIRPLANE
	if (planeSpeed - 150 < 300) {
		model = glm::translate(glm::mat4(1.0f), glm::vec3(-200.0f + planeSpeed, 50.0f, 1.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
		model = glm::rotate(model, glm::radians(angle + 90), glm::vec3(0, 1, 0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
		glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
		glUniform1i(isPoolLoc, 0);
		airplaneModel.Draw(myCustomShader);
	}
	//GRASS
	for (int i = 0; i < 1024; i++) {
		model = glm::translate(glm::mat4(1.0f),grassPositions[i]);
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
		glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
		grassModel[i].Draw(myCustomShader);
	}

	//FENCE
	model = glm::mat4(1.0f);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	fenceModel.Draw(myCustomShader);

	//SMALL LAMP
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.9f, -1.3f, -1.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	smallLampModel.Draw(myCustomShader);

	//COTTAGE
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -0.2f, -0.8f));
	model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	glUniform1i(isPoolLoc, 0);
	cottage.Draw(myCustomShader);


	//TREE
	for (int i = 0; i < 6; i++) {
		model = glm::translate(glm::mat4(1.0f), glm::vec3(-8.0f * (i + 2), 0.0f, -2.0f));
		model = glm::rotate(model, glm::radians(angle), glm::vec3(0, 1, 0));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
		glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
		glUniform1i(isPoolLoc, 0);
		tree[i].Draw(myCustomShader);
	}
	//GROUND
	model = glm::translate(glm::mat4(1.0f), modelPos);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform1i(isPoolLoc, 0);
	ground.Draw(myCustomShader);

	//POOL
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1000.0f, -100.05f, 0.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform1i(isPoolLoc,1);
	pool.Draw(myCustomShader);

	//TABLE
	model = glm::mat4(1.0f);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	tableModel.Draw(myCustomShader);

	//TRASH BIN
	model = glm::mat4(1.0f);
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	trashBinModel.Draw(myCustomShader);
	  
	//draw a white cube around the light
	lightShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
	model = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::translate(model, lightDir);
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	glUniform3fv(cameraPosLoc, 1, glm::value_ptr(myCamera.getCameraPosition()));
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
	lightCube.Draw(lightShader);

	mySkyBox.Draw(skyboxShader, view, projection);

	//nu imi arata antetu functiei la hover
	//nu imi face normale
	//pt pool imi tre shader separat cu reflexii si refraxii
	
}



int main(int argc, const char * argv[]) {

	initOpenGLWindow();
	initOpenGLState();
	initFBOs();
	initModels();
	initShaders();
	initUniforms();
	glCheckError();
	while (!glfwWindowShouldClose(glWindow)) {
		renderScene();
		glfwPollEvents();
		glfwSwapBuffers(glWindow);
	}
	glfwTerminate();
	return 0;
}
