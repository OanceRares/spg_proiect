//
//  Camera.cpp
//  Lab5
//
//  Created by CGIS on 28/10/2016.
//  Copyright © 2016 CGIS. All rights reserved.
//

#include "Camera.hpp"

namespace gps {
    
    Camera::Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget)
    {
        this->cameraPosition = cameraPosition;
        this->cameraTarget = cameraTarget;
        this->cameraDirection = glm::normalize(cameraTarget - cameraPosition);
        this->cameraRightDirection = glm::normalize(glm::cross(this->cameraDirection, glm::vec3(0.0f, 1.0f, 0.0f)));
		this->lockHeight = false;
    }
    
    glm::mat4 Camera::getViewMatrix()
    {
        return glm::lookAt(cameraPosition, cameraPosition + cameraDirection , glm::vec3(0.0f, 1.0f, 0.0f));
    }

	glm::vec3 Camera::getCameraTarget()
	{
		return cameraTarget;
	}

    glm::vec3 Camera::getCameraPosition() {
		return cameraPosition;
	}

	void Camera::setCameraTarget(glm::vec3 newCameraTarget) {
		this->cameraTarget = newCameraTarget;
	}

	void Camera::setCameraPosition(glm::vec3 newCameraPosition) {
		this->cameraPosition = newCameraPosition;
	}

	void Camera::lockYAxis() {
		this->lockHeight = true;
	}

    void Camera::move(MOVE_DIRECTION direction, float speed1)
    {
		float speed = speed1 * 1.5;

		glm::vec3 newPosition = cameraPosition;
	
		if (lockHeight == true) {
			switch (direction) {
			case MOVE_FORWARD:
				newPosition += cameraDirection * speed;
				break;

			case MOVE_BACKWARD:
				newPosition -= cameraDirection * speed;
				break;

			case MOVE_RIGHT:
				newPosition += cameraRightDirection * speed;
				break;

			case MOVE_LEFT:
				newPosition -= cameraRightDirection * speed;
				break;
			}
	
			newPosition.y = 2.5f;

			float x = newPosition.x;
			float y = newPosition.y;
			float z = newPosition.z;
			if (!((  (x < -59.5 || x > 62)   ||   (z > 62 || z < -62)  ) 
			|| (   (x > -4.5 && x < 14.5)  &&   (z > -5 && z < 4.1) )))
			{
				cameraPosition = newPosition;
			}
		}
	
	}
    
    void Camera::rotate(float pitch, float yaw)
    {
		cameraDirection.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
		cameraDirection.y = sin(glm::radians(pitch));
		cameraDirection.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
		cameraRightDirection  = glm::normalize(glm::cross(this->cameraDirection, glm::vec3(0.0f, 1.0f, 0.0f)));
	}


}
